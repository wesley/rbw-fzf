# rbw-fzf
This simple script makes use of [rbw](https://git.tozt.net/rbw), [fzf](https://github.com/junegunn/fzf) and [wl-clipboard](https://github.com/bugaevc/wl-clipboard) to get passwords, usernames and notes from your Bitwarden account without relying on the official clients (the official CLI client is slow and hard to use in my opinion).

# How to use it
Simply run `rbw-fzf` on your terminal, search for a Bitwarden entry and press Enter on your keyboard. You probably want to add this script to your `$PATH` 

# Warnings
- This script probably sucks, I'm not a software developer
- I use GNOME as my desktop environment and I don't have a clipboard history, so I don't care if wl-clipboard tries to clear it. You probably don't want this behaviour if you have a clipboard history
- I have no interest in adding support for others clipboard managers, but you should be able to do this easily
